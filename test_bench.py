import mpmath
guard_bits=80
import sys
samples_exponent=int(sys.argv[1])
fractional_bits=int(sys.argv[2])

class root_generation(Exception):
    pass

try:
    if samples_exponent<4:
        raise root_generation('There must be at least 16 samples')
    if fractional_bits<1:
        raise root_generation('The precision can not be lower than 1')
        
    f = open(sys.argv[3]+'/twiddle_test.v', 'w')
    rom_width=(3+fractional_bits)<<1#one bit for the sign, another to record of the l.s,bits are all zero, the rest for m.s. bits
    f.write("module test_ROM(output reg ["+str(rom_width-1)+":0] dout, input ["+str(samples_exponent-1)+":0] addr);\n\n")
    f.write("always @(addr)\n")
    f.write("case (addr)\n")
    precission=fractional_bits+guard_bits
    #maximun_value=(1<<fractional_bits)-1
    lsb_mask=(1<<guard_bits)-1
    data_width=str(fractional_bits+1)#integer bit and fractional bits
    addr_prefix=str(samples_exponent)+"'h"
    with mpmath.workprec(precission+8):
        for address in range(1<<samples_exponent):
            root=mpmath.expjpi(-mpmath.ldexp(address,1-samples_exponent))
            sin=int(mpmath.nint(mpmath.ldexp(root.imag,precission),prec=0))
            cosin=int(mpmath.nint(mpmath.ldexp(root.real,precission),prec=0))
            sin_sign="1'b"+str(int(sin<0))
            cosin_sign="1'b"+str(int(cosin<0))
            sin=abs(sin)
            cosin=abs(cosin)
            sin_lsb="1'b"+str(int(bool(sin&lsb_mask)))
            cosin_lsb="1'b"+str(int(bool(cosin&lsb_mask)))
            sin=data_width+"'h"+format(sin>>guard_bits,'x')
            cosin=data_width+"'h"+format(cosin>>guard_bits,'x')
            f.write(addr_prefix+format(address,'x')+":dout={"+sin_sign+","+sin+","+sin_lsb+","+cosin_sign+","+cosin+","+cosin_lsb+"};\n")
    f.write("endcase\n")
    f.write("endmodule\n")
    f.write("`define fractional_bits "+str(fractional_bits)+"\n")
    f.write("`define samples_exponent "+str(samples_exponent)+"\n")
    f.write("""
`timescale 1ns / 1ps

module twiddle_test;
	reg myclk;
	wire real_sign,imaginary_sign,control_real_sign,control_imaginary_sign;
	wire [-1:-`fractional_bits] real_magnitude,imaginary_magnitude;
    wire [0:-`fractional_bits] control_real_magnitude,control_imaginary_magnitude;
	wire control_real_lsb,control_imaginary_lsb;
	reg [`samples_exponent-1:0] sample_number;

	twiddle_calculator unit_under_test
	(	sample_number,
		imaginary_sign,
		imaginary_magnitude,
		real_sign,
		real_magnitude
	);
     
	test_ROM my_test_ROM
	(	{
		control_imaginary_sign,control_imaginary_magnitude,control_imaginary_lsb,
		control_real_sign,control_real_magnitude,control_real_lsb
		},sample_number
	);



	always #4 myclk = ~myclk;
	always	@(posedge myclk)
		sample_number<=sample_number+1;

	always	@(negedge myclk)
		begin
			if(control_real_sign!==real_sign && (real_magnitude || control_real_magnitude || control_real_lsb))
		 		$display("angle_number=$%h control_real_sign=%h real_sign=%h",sample_number,control_real_sign,real_sign);
			if(control_imaginary_sign!==imaginary_sign && (imaginary_magnitude || control_imaginary_magnitude || control_imaginary_lsb))
		 		$display("angle_number=$%h control_imaginary_sign=%h imaginary_sign=%h",sample_number,control_imaginary_sign,imaginary_sign);

			if( (real_magnitude>control_real_magnitude+1) || (control_real_magnitude+control_real_lsb>real_magnitude+1) )
		 		$display("angle_number=$%h control_real_magnitude=%h_%h real_magnitude=%h",sample_number,control_real_magnitude,control_real_lsb,real_magnitude);


			if( (imaginary_magnitude>control_imaginary_magnitude+1) || (control_imaginary_magnitude+control_imaginary_lsb>imaginary_magnitude+1) )
		 		$display("angle_number=$%h control_imaginary_magnitude=%h_%h imaginary_magnitude=%h",sample_number,control_imaginary_magnitude,control_imaginary_lsb,imaginary_magnitude);
		end
	initial begin
			//$dumpfile("twiddle_waves.vcd"); $dumpvars(0, twiddle_test);
			myclk=1'b1;
			sample_number=0;
			repeat (1<<`samples_exponent)	@(posedge myclk);
			$finish;
		end
endmodule
            """)
    f.close()
except IndexError:
    print("arguments: <samples exponent> <fractional bits> <target directory>\n")