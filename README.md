This software generates synthesizable Verilog descriptions of twiddle factor calculators and the corresponding testbenches. The real and imaginary parts are faithfully rounded. Sign-magnitude notation is used, and each mangnitude is represented in fixed-point format. The generators compute the complement of the cosine internally, and have a tree structure. The number of samples of the DFT must be a power of 2.

To generate the descriptions execute

>python3 twiddle.py samples_exponent fractional_bits number_of_stages target_directory

The first argument is the logarithm to base 2 of the number of samples. The second argument is the number of fractional bits used to represent the twiddle factors. The third argument is the height of the tree structure of the generator. The last argument is the target folder, and defaults to the current directory. Several descriptions will be generated that will differ in the size of their ROMs. Each description will be stored in the subfolder `twiddle_calculator_<argument1>_<argument2>_<argument3>/combination_<number>`

It is also possible to generate the corresponding testbench by executing

>python3 test_bench.py samples_exponent fractional_bits target_directory

The first argument is the logarithm to base 2 of the number of samples of the generator. The second argument is the number of fractional bits that the generator uses to represent the twiddle factors. The last argument is the name of target the folder.
