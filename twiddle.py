import mpmath
guard_bits=80

def root_of_unity( samples_exponent,order_number,precision=mpmath.mp.prec):
    """Returns a ( 2^samples_exponent)-th root of unity.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The returned root is the one with index order_number.
    """
    with mpmath.workprec(precision):
        return mpmath.expjpi(mpmath.ldexp(order_number,1-samples_exponent))

class fixed_point_root_of_unity:
    """Codes a ( 2^samples_exponent)-th root of unity in two integer fields.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The returned root x is the one with index order_number.
        The field 'sin' is the nearest integer to the value imaginary_part(x)*(2^fractional_bits)
        The field 'com' is the nearest integer to the value (1-real_part(x))*(2^fractional_bits)
    """
    def __init__(self,samples_exponent,order_number,fractional_bits=mpmath.mp.prec):
        root=root_of_unity(samples_exponent,order_number,fractional_bits+guard_bits)
        self.sin=int(mpmath.nint(mpmath.ldexp(root.imag,fractional_bits),prec=0))
        self.com=int(mpmath.nint(mpmath.ldexp(1-root.real,fractional_bits),prec=0))

class truncated_fixed_point_root_of_unity:
    """Codes a ( 2^samples_exponent)-th root of unity in two integer fields.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The returned root x is the one with index order_number.
        The field 'sin' is the nearest integer to the value imaginary_part(x)*(2^fractional_bits)
        The field 'com' is the nearest integer to the value (1-real_part(x))*(2^fractional_bits)
    """
    def __init__(self,samples_exponent,order_number,fractional_bits=mpmath.mp.prec):
        root=root_of_unity(samples_exponent,order_number,fractional_bits+guard_bits)
        self.sin=int(mpmath.ldexp(root.imag,fractional_bits))
        self.com=int(mpmath.ldexp(1-root.real,fractional_bits))

def fixed_point_roots_of_unity(samples_exponent,order_number_range,fractional_bits=mpmath.mp.prec):
    """Returns a list of fixed point representations of ( 2^samples_exponent)-th roots of unity.
        Lets be x one of the roots, its value is coded in a tuple (i,r)
        where i is an integer with the value imaginary_part(x)*(2^fractional_bits)
        and r is an integer with the value real_part(x)*(2^fractional_bits).
        The returned roots are those whose index are in the range order_number_range.
    """
    return [fixed_point_root_of_unity(samples_exponent,i,fractional_bits) for i in order_number_range]

twiddle_calculator_content="""

module twiddle_calculator
	(	input [`samples_exponent-1:0] d,
		output imaginary_sign,
		output [-1:-(`output_width)] imaginary_magnitude,
		output real_sign,
		output [-1:-(`output_width)] real_magnitude
	);
	wire [`output_width*2-1:0] data;
	wire [-1:-`output_width] sin, cosin,rom_sin,rom_cosin;
	wire [`samples_exponent-4:0] addr;
	assign addr= (d[`samples_exponent-4:0]	^	{(`samples_exponent-3){d[`samples_exponent-3]}})	+	d[`samples_exponent-3];
	trigonometric_calculator virtual_rom(addr,rom_sin,rom_cosin);
	assign {sin,cosin}= d[`samples_exponent-3] & ~|d[`samples_exponent-4:0]?
				{`inverse_sqrt_2,`inverse_sqrt_2} :
				{rom_sin,rom_cosin};
	assign real_sign=d[`samples_exponent-1]^d[`samples_exponent-2];
	assign imaginary_sign=~d[`samples_exponent-1];
	assign {imaginary_magnitude,real_magnitude}= d[`samples_exponent-2]^d[`samples_exponent-3] ? {cosin,sin} : {sin,cosin};
endmodule
"""

twiddle_performance_test_content="""
module register #(parameter width=1)
		(
			input clk,
			input [width-1:0] inputs,
			output reg [width-1:0] outputs
		);

	always @(posedge clk)
		outputs <= inputs;
endmodule

module top_level
	(	input clk,
		input [`samples_exponent-1:0] inputs,
		output [(`output_width+1)*2-1:0] outputs
	);

	wire [`samples_exponent-1:0] sampled_inputs;
	wire [(`output_width+1)*2-1:0] unsampled_outputs;

	register #(`samples_exponent) input_register(clk,inputs,sampled_inputs);
	register #((`output_width+1)*2) output_register(clk,unsampled_outputs,outputs);
	
	twiddle_calculator unit_analisys
	(	
		sampled_inputs,
		unsampled_outputs [`output_width*2],
		unsampled_outputs [`output_width-1:0],
		unsampled_outputs [`output_width*2+1],
		unsampled_outputs [`output_width*2-1:`output_width]
	);
endmodule
"""
report=""

import os

import sys

class ROM_info:
    """
    """
    def __init__(self,id):
        global number_of_big_memories,min_number_of_address_lines,samples_exponent,ROMS_precission,little_memory_depth
        self.id=id#must be in range(number_of_memories)
        maximun_number_of_big_memories_with_lower_id=min(id,number_of_big_memories)
        self.contents=[]#contens[i] will be the content of the ROM when there are i big memories with an index lower than self.id
        current_samples_exponent=samples_exponent-id*min_number_of_address_lines
        for i in range(maximun_number_of_big_memories_with_lower_id+1):#compute all the possible contents
        #i is the number of big memories with id lower than self.id
            content_can_be_big=(number_of_big_memories>i)
            current_depth=little_memory_depth<<content_can_be_big
            if not(i):#this is the first content generated
                current_content=fixed_point_roots_of_unity(current_samples_exponent,range(current_depth),ROMS_precission)
            else:
                current_content=[previous_content[x] for x in range(0,previous_depth,2)]
                if current_depth==previous_depth:#only a half of the content has been stored
                    for x in range(current_depth>>1,current_depth):
                        current_content.append(fixed_point_root_of_unity(current_samples_exponent,x,ROMS_precission))
            self.contents.append(current_content)
            previous_content=current_content
            previous_depth=current_depth
            current_samples_exponent-=1
            
def set_next_memory_combination():
    global big_memories_with_id_lower_than,is_big_memory
    memory_to_be_halved=0
    next_memory=1
    while not(is_big_memory[memory_to_be_halved]) or is_big_memory[next_memory]:
        memory_to_be_halved=next_memory
        next_memory+=1
    is_big_memory[next_memory]=True
    previous_big_memories=big_memories_with_id_lower_than[next_memory]-1
    big_memories_with_id_lower_than[next_memory]=previous_big_memories
    is_big_memory[:next_memory]=[(i<previous_big_memories) for i in range(next_memory)]
    big_memories_with_id_lower_than[:next_memory]=list(range(previous_big_memories))+[previous_big_memories]*(next_memory-previous_big_memories)
    
#class fixed_point_complex_product:
#    """
#    """
#    def __init__(self,number_of_output_bits,number_of_input_bits,A,B):
#        sin_A_plus_B=((A.sin+B.sin)<<number_of_input_bits)-A.sin*B.com-B.sin*A.com        
#        com_A_plus_B=((A.com+B.com)<<number_of_input_bits)-A.com*B.com+B.sin*A.sin
#        displacements=(number_of_input_bits<<1)-number_of_output_bits
#        if(displacements>0):
#            lost_bit_position=1<<(displacements-1)
#            sin_A_plus_B=sin_A_plus_B+ (sin_A_plus_B & lost_bit_position)
#            sin_A_plus_B=sin_A_plus_B>>displacements
#            com_A_plus_B=com_A_plus_B+ (com_A_plus_B & lost_bit_position)        
#            com_A_plus_B=com_A_plus_B>>displacements
#        self.sin=sin_A_plus_B
#        self.com=com_A_plus_B   

class tree:
    """
    """
    def __init__(self,depth,id):
        global min_number_of_address_lines,big_memories_with_id_lower_than,samples_exponent
        global number_of_memories,number_of_input_lines,fractional_bits,number_of_stages
        if depth:#the tree has branches
            sons_depth=depth-1
            first_son=id<<1
            self.sons=[tree(sons_depth,first_son),tree(sons_depth,first_son+1)]
        else:
            self.sons=[]
        self.precission=fractional_bits+(number_of_stages-depth)*2
        self.id=id
        self.depth=depth
        number_of_descendant_ROMS=1<<depth
        first_descendant_ROM=id<<depth
        first_input_line=first_descendant_ROM*min_number_of_address_lines+big_memories_with_id_lower_than[first_descendant_ROM]
        next_non_descendant_ROM=first_descendant_ROM+number_of_descendant_ROMS
        if next_non_descendant_ROM==number_of_memories:#there aren't non descendant ROMs with higher id
            next_non_descendant_input_line=number_of_input_lines
        else:
            next_non_descendant_input_line=next_non_descendant_ROM*min_number_of_address_lines+big_memories_with_id_lower_than[next_non_descendant_ROM]
        tree_number_of_input_lines=next_non_descendant_input_line-first_input_line
        self.last_root=fixed_point_root_of_unity(samples_exponent-first_input_line,(1<<tree_number_of_input_lines)-1,self.precission)
        self.sin_is_zero=not(self.last_root.sin)
        self.com_is_zero=not(self.last_root.com)
        if depth:#the tree is not a leaf
#            sons_depth=depth-1
#            first_son_id=id<<1
#            self.sons=[tree(sons_depth,first_son_id),tree(sons_depth,first_son_id+1)]
            truncated_last_root=truncated_fixed_point_root_of_unity(samples_exponent-first_input_line,(1<<tree_number_of_input_lines)-1,self.precission)
            if not(self.sin_is_zero):
                self.last_root.sin=truncated_last_root.sin+1
            if not(self.com_is_zero):
                self.last_root.com=truncated_last_root.com+1
#        else:
#            self.sons=[]
        self.sin_width=self.last_root.sin.bit_length()
        self.com_width=self.last_root.com.bit_length()
        
    def define_wires(self):
        global trigonometric_content,number_of_stages
        last_bit_index_str=str(-self.precission)
        identifier="_"+str(self.depth)+"_"+str(self.id)
        if self.depth==number_of_stages:#this is the root node
            trigonometric_content+="wire [-1:"+last_bit_index_str+"] sin"+identifier+";\n"
            trigonometric_content+="wire [-1:"+last_bit_index_str+"] minus_com"+identifier+";\n"
        else:
            if not(self.sin_is_zero):
                sin_first_index=str(self.sin_width-1-self.precission)
                bus_name="sin"+identifier
                trigonometric_content+="wire ["+sin_first_index+":"+last_bit_index_str+"] "+bus_name+";\n"
#                trigonometric_content+="`define "+bus_name+" "+bus_name+"\n"
#            else:
#                trigonometric_content+="`define "+bus_name+" "+str(self.sin_value)+"\n"
            if not(self.com_is_zero):
                com_first_index=str(self.com_width-1-self.precission)
                bus_name="com"+identifier
                trigonometric_content+="wire ["+com_first_index+":"+last_bit_index_str+"] "+bus_name+";\n"
#                trigonometric_content+="`define "+bus_name+" "+bus_name+"\n"
        if not(self.sin_is_zero and self.com_is_zero):
            for son in self.sons:
                    son.define_wires()
                    
    def define_componentes(self):
        global min_number_of_address_lines,big_memories_with_id_lower_than,is_big_memory,trigonometric_content
        if not(self.sin_is_zero and self.com_is_zero):
            identifier="_"+str(self.depth)+"_"+str(self.id)
            if not(self.depth):#the component is a ROM        
                first_input_line=self.id*min_number_of_address_lines+big_memories_with_id_lower_than[self.id]
                last_input_line=first_input_line+min_number_of_address_lines+is_big_memory[self.id]-1
                trigonometric_content+="ROM"+str(self.id)+" myrom"+identifier
                trigonometric_content+="({sin"+identifier
                if not(self.com_is_zero):
                    trigonometric_content+=",com"+identifier
                trigonometric_content+="},d["+str(last_input_line)+":"+str(first_input_line)+"]);\n"
                self.define_rom_module()
            else:#the component is a complex multiplier
                self.define_complex_multiplier()
                for son in self.sons:
                        son.define_componentes()
            
    def define_complex_multiplier(self):
        global number_of_stages,trigonometric_content,report
        SA=self.sons[1]
        SB=self.sons[0]
        is_root_node= self.depth==number_of_stages
        if is_root_node:
            sin_first_index="-1"
            com_first_index="-1"
        else:
            sin_first_index=str(self.sin_width-1-self.precission)
            com_first_index=str(self.com_width-1-self.precission)
        last_bit_index=-self.precission
        last_bit_index_str=str(last_bit_index)
        internal_last_bit_index=-((self.precission+2)*2)
        internal_last_bit_index_str=str(internal_last_bit_index)
        sons_last_bit_index=-self.precission-2
        sons_prefix="_"+str(self.depth-1)+"_"
        identifier="_"+str(self.depth)+"_"+str(self.id)
        SB_identifier=sons_prefix+str(SB.id)
        SA_identifier=sons_prefix+str(SA.id)
        
#        sinA_first_index=sons_last_bit_index+SA.sin_width-1
#        comA_first_index=sons_last_bit_index+SA.com_width-1
#        sinB_first_index=sons_last_bit_index+SB.sin_width-1
#        comB_first_index=sons_last_bit_index+SB.com_width-1
        report+="level "+str(self.depth)+", node "+str(self.id)+":\n"
        report+="\t size of multiplier for SIN(A)xSIN(B): "+str(SA.sin_width)+"x"+str(SB.sin_width)+"\n"
        report+="\t size of multiplier for SIN(A)xCOM(B): "+str(SA.sin_width)+"x"+str(SB.com_width)+"\n"
        report+="\t size of multiplier for COM(A)xSIN(B): "+str(SA.com_width)+"x"+str(SB.sin_width)+"\n"
        report+="\t size of multiplier for COM(A)xCOM(B): "+str(SA.com_width)+"x"+str(SB.com_width)+"\n"
        
        if (SA.sin_is_zero):
            trigonometric_content+="`define sin"+SA_identifier+" 0\n"
        else:
            trigonometric_content+="`define sin"+SA_identifier+" sin"+SA_identifier+"\n"
        if (SA.com_is_zero):
            trigonometric_content+="`define com"+SA_identifier+" 0\n"
        else:
            trigonometric_content+="`define com"+SA_identifier+" com"+SA_identifier+"\n"
        if (SB.sin_is_zero):
            trigonometric_content+="`define sin"+SB_identifier+" 0\n"
        else:
            trigonometric_content+="`define sin"+SB_identifier+" sin"+SB_identifier+"\n"
        if (SB.com_is_zero):
            trigonometric_content+="`define com"+SB_identifier+" 0\n"
        else:
            trigonometric_content+="`define com"+SB_identifier+" com"+SB_identifier+"\n"
            
        max_sinAcomB=SA.last_root.sin*SB.last_root.com
        sinAcomB_width=max_sinAcomB.bit_length()
        trigonometric_content+="wire ["+str(sinAcomB_width+internal_last_bit_index-1)+":"+internal_last_bit_index_str+"] sinAcomB"+identifier+";\n"
        max_comAsinB=SA.last_root.com*SB.last_root.sin
        comAsinB_width=max_comAsinB.bit_length()
        trigonometric_content+="wire ["+str(comAsinB_width+internal_last_bit_index-1)+":"+internal_last_bit_index_str+"] comAsinB"+identifier+";\n"
        max_sinAcomB_PLUS_comAsinB=max_sinAcomB+max_comAsinB
        sinAcomB_PLUS_comAsinB_width=max_sinAcomB_PLUS_comAsinB.bit_length()
        sinAcomB_PLUS_comAsinB_first_index=sinAcomB_PLUS_comAsinB_width+internal_last_bit_index-1
        trigonometric_content+="wire ["+str(sinAcomB_PLUS_comAsinB_first_index)+":"+internal_last_bit_index_str+"] sinAcomB_PLUS_comAsinB"+identifier+";\n"
        
        max_comAcomB=SA.last_root.com*SB.last_root.com
        comAcomB_width=max_comAcomB.bit_length()        
        trigonometric_content+="wire ["+str(comAcomB_width+internal_last_bit_index-1)+":"+internal_last_bit_index_str+"] comAcomB"+identifier+";\n"
        max_sinAsinB=SA.last_root.sin*SB.last_root.sin
        sinAsinB_width=max_sinAsinB.bit_length()
        trigonometric_content+="wire ["+str(sinAsinB_width+internal_last_bit_index-1)+":"+internal_last_bit_index_str+"] sinAsinB"+identifier+";\n"
        
        if is_root_node:
            trigonometric_content+="wire [-1:"+internal_last_bit_index_str+"] comAcomB_MINUS_sinAsinB"+identifier+";\n"
        else:
            trigonometric_content+="wire ["+str(sinAsinB_width+internal_last_bit_index-1)+":"+internal_last_bit_index_str+"] sinAsinB_MINUS_comAcomB"+identifier+";\n"
        
        trigonometric_content+="wire ["+sin_first_index+":"+str(sons_last_bit_index)+"] accurate_sin"+identifier+";\n"
        trigonometric_content+="wire ["+com_first_index+":"+str(sons_last_bit_index)+"] accurate_"
        if is_root_node:
            trigonometric_content+="minus_"
        trigonometric_content+="com"+identifier+";\n"
        
        trigonometric_content+="assign sinAcomB"+identifier+"="
        if SA.sin_is_zero or SB.com_is_zero:
            trigonometric_content+="0;\n"
        else:
            trigonometric_content+="`sin"+SA_identifier+"*"+"`com"+SB_identifier+";\n"
        
        trigonometric_content+="assign comAsinB"+identifier+"="
        if SA.com_is_zero or SB.sin_is_zero:
            trigonometric_content+="0;\n"
        else:
            trigonometric_content+="`com"+SA_identifier+"*"+"`sin"+SB_identifier+";\n"
        
        trigonometric_content+="assign comAcomB"+identifier+"="
        if SA.com_is_zero or SB.com_is_zero:
            trigonometric_content+="0;\n"
        else:
            trigonometric_content+="`com"+SA_identifier+"*"+"`com"+SB_identifier+";\n"
        
        trigonometric_content+="assign sinAsinB"+identifier+"="
        if SA.sin_is_zero or SB.sin_is_zero:
            trigonometric_content+="0;\n"
        else:
            trigonometric_content+="`sin"+SA_identifier+"*"+"`sin"+SB_identifier+";\n"
        
        trigonometric_content+="assign sinAcomB_PLUS_comAsinB"+identifier+"=sinAcomB"+identifier+"+comAsinB"+identifier+";\n"
        if is_root_node:
            trigonometric_content+="assign comAcomB_MINUS_sinAsinB"+identifier+"=comAcomB"+identifier+"-sinAsinB"+identifier+";\n"
        else:
            trigonometric_content+="assign sinAsinB_MINUS_comAcomB"+identifier+"=sinAsinB"+identifier+"-comAcomB"+identifier+";\n"
            
        trigonometric_content+="assign accurate_sin"+identifier+"=`sin"+SA_identifier+"+`sin"+SB_identifier
        sinAcomB_PLUS_comAsinB_first_index=sinAcomB_PLUS_comAsinB_width+internal_last_bit_index-1
        trigonometric_content+="-("
        if sinAcomB_PLUS_comAsinB_first_index>=sons_last_bit_index:
            trigonometric_content+="sinAcomB_PLUS_comAsinB"+identifier+"["+str(sinAcomB_PLUS_comAsinB_first_index)+":"+str(sons_last_bit_index)+"]+"
        trigonometric_content+="|sinAcomB_PLUS_comAsinB"+identifier+"["+str(min(sinAcomB_PLUS_comAsinB_first_index,sons_last_bit_index-1))+":"+internal_last_bit_index_str+"]);\n"

        if is_root_node:
            trigonometric_content+="assign accurate_minus_com"+identifier+"=comAcomB_MINUS_sinAsinB"+identifier
            trigonometric_content+="[-1:"+str(sons_last_bit_index)+"]-("+"`com"+SA_identifier+"+"+"com"+SB_identifier+");\n"
        else:
            trigonometric_content+="assign accurate_com"+identifier+"=`com"+SA_identifier+"+`com"+SB_identifier
            sinAsinB_MINUS_comAcomB_first_index=sinAsinB_width+internal_last_bit_index-1
            if sinAsinB_MINUS_comAcomB_first_index>=sons_last_bit_index:
                trigonometric_content+="+sinAsinB_MINUS_comAcomB"+identifier+"["+str(sinAsinB_MINUS_comAcomB_first_index)+":"+str(sons_last_bit_index)+"]"
            trigonometric_content+=";\n"

        trigonometric_content+="assign sin"+identifier+"=accurate_sin"+identifier+"["+sin_first_index+":"+last_bit_index_str+"]+accurate_sin"+identifier+"["+str(last_bit_index-1)+"];\n"

        if is_root_node:
            trigonometric_content+="assign minus_com"+identifier+"=accurate_minus_com"+identifier+"["+com_first_index+":"+last_bit_index_str+"]+accurate_minus_com"+identifier+"["+str(last_bit_index-1)+"];\n"
        else:
            trigonometric_content+="assign com"+identifier+"=accurate_com"+identifier+"["+com_first_index+":"+last_bit_index_str+"]+accurate_com"+identifier+"["+str(last_bit_index-1)+"];\n"
            
    def define_rom_module(self):
        global little_memory_depth,ROMS_info,big_memories_with_id_lower_than,min_number_of_address_lines,is_big_memory,comb_subdirectory,report,total_ROMs_size
        big_rom=is_big_memory[self.id]
        rom_depth=little_memory_depth<<big_rom
        content=ROMS_info[self.id].contents[big_memories_with_id_lower_than[self.id]]
        sin_width=self.sin_width
        com_width=self.com_width
        rom_width=sin_width+com_width
        module_name='ROM'+str(self.id)
        f = open(comb_subdirectory+'/'+module_name+'.v', 'w')
        f.write("module "+module_name+"(output reg ["+str(rom_width-1)+":0] dout, input ["+str(min_number_of_address_lines+big_rom-1)+":0] addr);\n")
        f.write("always @(addr) begin\n")
        f.write("    case (addr)\n")
        total_ROMs_size+=rom_width*rom_depth
        rom_width=str(rom_width)  
        for address in range(rom_depth):
            root=content[address]
            f.write(str(address)+": dout="+rom_width+"'h"+format((root.sin<<com_width)+root.com,'x')+";\n")
        f.write("    endcase\n")
        f.write("end\n")
        f.write("endmodule\n")
        f.close()
        report+="ROM "+str(self.id)+": width="+rom_width+", depth="+str(rom_depth)+"\n"
##end of tree class definition
    
class files_generation(Exception):
    pass

def generate_verilog_files():
    global samples_exponent,fractional_bits,number_of_stages,number_of_memories,number_of_input_lines
    global number_of_big_memories,min_number_of_address_lines,little_memory_depth,report
    global samples_exponent,ROMS_precission,big_memories_with_id_lower_than,is_big_memory,ROMS_info
    global twiddle_calculator_content,trigonometric_content,twiddle_performance_test_content,total_ROMs_size
    if samples_exponent<4:
        raise files_generation('There must be at least 16 samples')
    if fractional_bits<1:
        raise files_generation('The precision can not be lower than 1')
    number_of_input_lines=samples_exponent-3
    maximun_number_of_stages=number_of_input_lines.bit_length()-1 #this is floor(log2(number_of_input_lines))
    if number_of_stages>maximun_number_of_stages:
        raise files_generation('The maximun number of stages for this number of samples is '+str(maximun_number_of_stages))
    if number_of_stages<1:
        raise files_generation('The minimun number of stages is 1')
    if not os.path.exists(target_directory):
        os.makedirs(target_directory)
    number_of_memories=1<<number_of_stages
    number_of_big_memories=number_of_input_lines%number_of_memories
    number_of_little_memories=number_of_memories-number_of_big_memories
    min_number_of_address_lines=int(number_of_input_lines/number_of_memories)
    ROMS_precission=fractional_bits+(number_of_stages<<1)
    little_memory_depth=1<<min_number_of_address_lines
    ROMS_info=[ROM_info(x) for x in range(number_of_memories)]
    number_of_combinations=int(mpmath.binomial(number_of_memories,number_of_big_memories))
    with mpmath.workprec(fractional_bits+guard_bits):
        inverse_sqrt_2=int(mpmath.nint(mpmath.ldexp(1/mpmath.sqrt(2),fractional_bits),prec=0))
    verilog_defines="`define samples_exponent "+str(samples_exponent)+"\n"
    verilog_defines+="`define output_width "+str(fractional_bits)+"\n"
    twiddle_performance_test_content=verilog_defines+twiddle_performance_test_content
    verilog_defines+="`define inverse_sqrt_2 "+str(fractional_bits)+"'d"+str(inverse_sqrt_2)+"\n"
    twiddle_calculator_content=verilog_defines+twiddle_calculator_content
    for current_comb_id in range(number_of_combinations):
        report+="\n\n Resources of combination "+str(current_comb_id)+":\n"
        total_ROMs_size=0
        if not(current_comb_id):#first combination of big memories
            is_big_memory=[(number_of_big_memories>i) for i in range(number_of_memories)]
            big_memories_with_id_lower_than=list(range(number_of_big_memories))+[number_of_big_memories]*number_of_little_memories
            [is_big_memory[i]*i for i in range(number_of_memories)]
        else:
            set_next_memory_combination()
        generate_current_combination_code(current_comb_id)
        report+="Total size of the required memories: "+str(total_ROMs_size)+" bits \n"
    create_file('report.txt',report)

def define_module(module_name,file_content,subdirectory='.'):
    create_file(module_name+'.v',file_content,subdirectory)
    
def create_file(file_name,file_content,subdirectory='.'):
    global target_directory
    f = open(target_directory+'/'+subdirectory+'/'+file_name, 'w')
    f.write(file_content)
    f.close()

def generate_current_combination_code(current_comb_id):
    global number_of_memories,target_directory,little_memory_depth,current_last_roots,trigonometric_content
    global ROMS_precission,number_of_stages,min_number_of_address_lines,fractional_bits,comb_subdirectory
    subdirectory="combination_"+str(current_comb_id)
    comb_subdirectory=target_directory+"/"+subdirectory
    if not os.path.exists(comb_subdirectory):
        os.makedirs(comb_subdirectory)
    current_tree=tree(number_of_stages,0)
    trigonometric_content="module trigonometric_calculator(input ["+str(number_of_input_lines-1)+":0] d, output [-1:-"+str(fractional_bits)+"] sin,cosin);\n"
    current_tree.define_wires()
    current_tree.define_componentes()
    level_name=str(number_of_stages)
    ###escribe las sentencias assign y crea los ficheros
    trigonometric_content+="assign sin=sin_"+level_name+"_0;\n"
    trigonometric_content+="assign cosin=minus_com_"+level_name+"_0[-1] ? minus_com_"+level_name+"_0 : -1 ;\n"
    trigonometric_content+="endmodule\n"
    define_module('trigonometric_calculator',trigonometric_content,subdirectory)
    define_module('twiddle_calculator',twiddle_calculator_content,subdirectory)
    define_module('performance_test',twiddle_performance_test_content,subdirectory)
##########################################
if len(sys.argv)>3:
    samples_exponent=int(sys.argv[1])
    fractional_bits=int(sys.argv[2])
    number_of_stages=int(sys.argv[3])
    if len(sys.argv)>4:
        target_directory=sys.argv[4]
    else:
        target_directory="."
    target_directory=target_directory+"/"+"twiddle_calculator_"+sys.argv[1]+"_"+sys.argv[2]+"_"+sys.argv[3]    
    generate_verilog_files()
else:
    print("arguments: <samples exponent> <fractional bits> <number of stages> [<target directory>]\n")
#    #for test
#    samples_exponent=int(8)
#    fractional_bits=int(16)
#    number_of_stages=int(2)
#    target_directory="prueba2"+"/"+"twiddle_calculator_"+str(samples_exponent)+"_"+str(fractional_bits)+"_"+str(number_of_stages)
#    generate_verilog_files()
